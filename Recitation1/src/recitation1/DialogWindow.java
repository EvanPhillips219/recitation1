
package recitation1;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * This class is a simple dialog window for alerting users of changes and or 
 * problems that occur.
 * @author Evan Phillips
 */
public class DialogWindow extends Scene {
    static final double W = 250;
    static final double H = 150;
    private Stage stage;
    private VBox vb;
    private Label text;
    private Button button;
    private String title; 
    private String message;
    
    /**
     *
     * @param title
     * @param message
     */
    public DialogWindow(String title, String message) {
        super(new VBox(30), 300, 200);
        this.title = title;
        this.message = message;
        buildNewObjects();
        action();
        showDialog();
    }
    
    /**
     * This method builds all the new objects
     */
    private void buildNewObjects() {
        stage = new Stage();
        text = new Label(message);
        button = new Button("OK");
        vb = (VBox)getRoot();
    }

    /**
     * This method sets the nodes and shows the stage
     */
    private void showDialog() {
        stage.setTitle(title);
        stage.setScene(this);
        stage.initModality(Modality.APPLICATION_MODAL);
        vb.setAlignment(Pos.CENTER);
        vb.getChildren().addAll(text, button);
        stage.show();
    }
    
    /**
     * This method returns the ok button
     * 
     * @return The button
     */
    public Button getButton() {
        return button;
    }
 
    /**
     * This method runs the button actions
     */
    public void action() {
        button.setOnAction(e -> {
            stage.close();
        });
    }
}